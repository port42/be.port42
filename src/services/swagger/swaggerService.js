/* eslint-disable class-methods-use-this */
const swaggerDocument = require('../../../swagger.json');
const applicationPackage = require('../../../package.json');

export default class SwaggerService {
    constructor(config) {
        this.config = config;
    }

    getData() {
        swaggerDocument.info.version = applicationPackage.version;
        swaggerDocument.info.title = applicationPackage.name;
        swaggerDocument.host = `${this.config.server.serverName}:${this.config.server.port}`;
        swaggerDocument.info.description = applicationPackage.description;
        return swaggerDocument;
    }
}
