export default (config, service) => ({
    getSwaggerDocument: () => service.swaggerService.getData(),
});
