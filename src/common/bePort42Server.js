import express from 'express';
import listEndpoints from 'express-list-endpoints';
import expressOasGenerator from 'express-oas-generator';
import registerModules from '../utils/registrator';
import Logger from '../utils/logger';
import apollo from '../graphql';

const ExpressServer = (
    app,
    port,
    shutdownPort,
) => {
    let serverInstance = null;
    let shutdownListener = null;

    function showAllRoutes() {
        Logger.debug(JSON.stringify(listEndpoints(app)));
    }

    const serverStopExecutor = (resolve) => {
        shutdownListener.close(() => {
            Logger.debug('shutdown listener stopped');
            serverInstance.close(() => {
                Logger.info('server stopped');
                resolve();
            });
        });
    };

    function stopServer() {
        if (!serverInstance) {
            Logger.debug('cannot stop server: not running');
            return Promise.resolve();
        }
        Logger.debug('stopping server be.Port42...');
        return new Promise(serverStopExecutor);
    }

    const serverStartExecutor = (resolve) => {
        serverInstance = app.listen(port, () => {
            Logger.info('server listening');
            Logger.debug(`on port ${port}`);
            showAllRoutes();
            const shutdownListenerApp = express();
            shutdownListenerApp.get('/shutdown', (req, res) => {
                res.setHeader('Connection', 'close');
                res.send('OK');
                setTimeout(stopServer, 0);
            });

            shutdownListener = shutdownListenerApp.listen(shutdownPort, () => {
                Logger.debug(`server listening for shutdown on port ${shutdownPort}`);
                resolve();
            });
        });
    };

    function startServer() {
        if (serverInstance) {
            Logger.debug('cannot start server: already running');
            return Promise.resolve();
        }
        Logger.debug('starting server...');
        return new Promise(serverStartExecutor);
    }

    function getAllRoutes() {
        return Promise.resolve(listEndpoints(app));
    }

    function getApp() {
        return Promise.resolve(app);
    }

    return ({
        start: startServer,
        stop: stopServer,
        showAllRoutes,
        getAllRoutes,
        getApp,
    });
};

const ServerFactoryFactory = ({
    modules, config, port, shutdownPort,
}) => (service) => {
    const app = express();
    // register the routes of the active modules
    registerModules(app, modules, service, config);
    expressOasGenerator.init(app, {});
    if (!config.server.dontUseGraphql) {
        apollo(app, config.server.graphqlPath);
    }
    return ExpressServer(app, port, shutdownPort, service);
};

export default ServerFactoryFactory;
