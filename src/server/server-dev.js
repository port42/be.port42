/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpack from 'webpack';
import Logger from '../utils/logger';
import config from '../config';
import BePort42Factory from '../common/bePort42Server';
import BePort42Service from '../services/bePort42Service';
import GlobalModule from '../modules/global/globalModule';
import SwaggerModule from '../modules/swagger/swaggerModule';
import RootModule from '../modules/root/rootModule';
import TestAuthModule from '../modules/testAuth/testAuthModule';
import webpackConfig from '../../webpack.dev.config.js';

Logger.info('Environment setup:');
Logger.info(`ENV                ${config.env}`);
Logger.info(`PORT               ${config.port}`);
Logger.info(`SERVER_NAME        ${config.serverName}`);
Logger.info(`API_URL_BASE       ${config.apiUrlBase}`);
Logger.info(`SHUTDOWN_PORT      ${config.shutdownPort}`);
Logger.info(`SHUTDOWN_TIMEOUT   ${config.shutdownTimeout}`);
Logger.info(`CORS_DOMAIN        ${config.corsDomain}`);
Logger.info(`DONT_USE_GRAPHQL   ${config.dontUseGraphql}`);
Logger.info(`GRAPHQL_PATH       ${config.graphqlPath}`);
Logger.info(`SWAGGER_PATH       ${config.swaggerPath}`);
Logger.info(`HTTP_SCHEMA        ${config.httpSchema}`);

const APP_CONFIG = {
    server: {
        port: config.port,
        shutdownPort: config.shutdownPort,
        shutdownTimeout: config.shutdownTimeout,
        serverName: config.serverName,
        dontUseGraphql: config.dontUseGraphql,
        corsDomain: config.corsDomain,
        graphqlPath: config.graphqlPath,
        swaggerPath: config.swaggerPath,
        apiUrlBase: config.apiUrlBase,
        httpSchema: config.httpSchema,
    },
    routes: undefined,
};
const ServerFactory = BePort42Factory({
    modules: {
        '/': RootModule,
        '/api/v1/global': GlobalModule,
        '/doc': SwaggerModule,
        '/testauth': TestAuthModule,
    },
    config: APP_CONFIG,
    port: APP_CONFIG.server.port,
    shutdownPort: APP_CONFIG.server.shutdownPort,
});

const startBePort42Server = server => server.start().then(() => server);

const setRoutes = (server) => {
    server.getAllRoutes().then((routes) => {
        config.routes = routes;
    });
    return server;
};

const setHotReload = (server) => {
    server.getApp().then((app) => {
        const compiler = webpack(webpackConfig);
        app.use(webpackDevMiddleware(compiler, {
            publicPath: webpackConfig.output.publicPath,
        }));
        app.use(webpackHotMiddleware(compiler));
    });
    return server;
};

const waitForShutdown = (server) => {
    const stopServer = () => {
        server
            .stop()
            .then(() => {
                // eslint-disable-next-line no-use-before-define
                clearTimeout(shutdwonTimeoutId);
            });
        const shutdwonTimeoutId = setTimeout(() => {
            Logger.debug('shutdown timeout reached - exiting');
            process.exit();
        }, APP_CONFIG.server.shutdownTimeout);
    };

    // listen for TERM signal .e.g. kill
    process.on('SIGTERM', stopServer);

    // listen for INT signal e.g. Ctrl-C
    process.on('SIGINT', stopServer);
};

// eslint-disable-next-line no-shadow
const buildStructure = config => ServerFactory(new BePort42Service(config));

Promise.resolve(APP_CONFIG)
    .then(buildStructure)
    .then(setHotReload)
    .then(startBePort42Server)
    .then(setRoutes)
    .then(waitForShutdown)
    .catch((e) => {
        Logger.error('Unhandled exception during server initialization:', e);
        process.exit(-1);
    });
