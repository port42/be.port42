/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */

const moduleLoader = (module, service, config) => module(config, service);

export default moduleLoader;
