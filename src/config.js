import dotenv from 'dotenv';
import constants from './utils/constants';

dotenv.config();

const commonConfig = {
    env: process.env.NODE_ENV || 'development',
    apiUrlBase: process.env.API_URL_BASE || constants.apiDefaultBasePath,
    port: parseInt(process.env.PORT, 10) || 4998,
    shutdownPort: parseInt(process.env.SHUTDOWN_PORT, 10) || 4999,
    shutdownTimeout: parseInt(process.env.SHUTDOWN_TIMEOUT, 10) || 2000,
    corsDomain: process.env.CORS_DOMAIN || '*',
    serverName: process.env.SERVER_NAME || 'localhost',
    dontUseGraphql: !!+process.env.DONT_USE_GRAPHQL || false,
    graphqlPath: process.env.GRAPHQL_PATH || constants.graphqlDefaultPath,
    swaggerPath: process.env.SWAGGER_PATH || constants.swaggerDefaultPath,
    httpSchema: process.env.HTTP_SCHEMA || constants.httpDefaultSchema,
};

export default commonConfig;
